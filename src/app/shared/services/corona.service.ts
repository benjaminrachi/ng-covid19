import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CoronaService {

  constructor(private http: HttpClient) { }

  getSummaryData() {
    return this.http.get('https://corona.lmao.ninja/v2/all');
  }

  getHistory() {
    return this.http.get('https://corona.lmao.ninja/v2/historical/all?lastdays=all');
  }
}
