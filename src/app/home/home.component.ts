import { Component, OnInit } from '@angular/core';
import { CoronaService } from '../shared/services/corona.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Chart } from 'chart.js';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  summaryData$: Observable<any>;
  historyData: any;
  labelsDates = new Array();
  chart;

  constructor(private coronaService: CoronaService) {}

  ngOnInit(): void {
    this.getSummaryData();
    this.getHistory();
  }

  getSummaryData() {
    this.summaryData$ = this.coronaService
      .getSummaryData()
      .pipe(tap((val) => console.log('summaryData', val)));
  }

  getHistory() {
    this.coronaService.getHistory().subscribe((val) => {
      this.historyData = val;
      console.log('history', val);
      this.drawHistoryChart();
    });
  }

  drawHistoryChart() {
    this.setLabelsDate();
    if (this.chart != undefined) {
      this.chart.destroy();
    }
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: Object.keys(this.historyData?.cases),
        datasets: [
          {
            label: 'Confimed Cases: ',
            data: Object.values(this.historyData?.cases),
            backgroundColor: '#fff',
            borderColor: 'rgba(234, 11, 11, 1)',
            pointBackgroundColor: 'rgba(234, 11, 11, 1)',
            pointBorderColor: 'rgba(234, 11, 11, 1)',
            pointHoverBackgroundColor: 'rgba(234, 11, 11, 1)',
            pointHoverBorderColor: 'rgba(234, 11, 11, 1)',
            fill: true,
            pointRadius: 0.4,
          },
          {
            label: 'Recovered Cases: ',
            data: Object.values(this.historyData?.recovered),
            backgroundColor: '#fff',
            borderColor: 'rgba(2, 182, 23, 1)',
            pointBackgroundColor: 'rgba(2, 182, 23, 1)',
            pointBorderColor: 'rgba(2, 182, 23, 1)',
            pointHoverBackgroundColor: 'rgba(2, 182, 23, 1)',
            pointHoverBorderColor: 'rgba(2, 182, 23, 1)',
            fill: true,
            pointRadius: 0.4,
          },
          {
            label: 'Deaths: ',
            data: Object.values(this.historyData?.deaths),
            backgroundColor: '#fff',
            borderColor: 'rgb(103, 58, 183)',
            pointBackgroundColor: 'rgb(103, 58, 183)',
            pointBorderColor: 'rgb(103, 58, 183)',
            pointHoverBackgroundColor: 'rgb(103, 58, 183)',
            pointHoverBorderColor: 'rgb(103, 58, 183)',
            fill: true,
            pointRadius: 0.4,
          },
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        title: {
          display: true,
          text: 'DAILY CASES WORLDWIDE',
          fontSize: 25,
          fontFamily: 'Sens',
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          position: 'average',
          caretPadding: 5,
        },
        hover: {
          intersect: false,
        },
        legend: {
          position: 'bottom',
          display: true,
        },
        scales: {
          xAxes: [
            {
              type: 'time',
              time: {
                unit: 'day',
                tooltipFormat: 'MMM DD',
                stepSize: 7,
              },
              gridLines: {
                drawOnChartArea: false,
              },
              display: true,
            },
          ],
          yAxes: [
            {
              type: 'linear',
              ticks: {
                beginAtZero: true,
                max: undefined,
              },
              gridLines: {
                drawOnChartArea: false,
              },
              display: true,
            },
          ],
        },
      },
    });
  }

  setLabelsDate() {
    if (this.historyData && this.historyData.cases) {
      this.labelsDates = Object.keys(this.historyData.cases);
    }
  }
}
