import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
})
export class FaqComponent implements OnInit {
  questions = [];
  constructor() {}

  ngOnInit(): void {
    this.questions = [
      {
        title: 'What is COVID-19?',
        answer: `COVID-19 is the infectious disease caused by the most recently
        discovered coronavirus. This new virus and disease were unknown before
        the outbreak began in Wuhan, China, in December 2019.`,
      },
      {
        title: 'What are the symptoms of COVID-19?',
        answer: `The most common symptoms of COVID-19 are fever, tiredness, and dry cough.
        Some patients may have aches and pains, nasal congestion, runny nose, sore throat or diarrhea.
        These symptoms are usually mild and begin gradually. Some people become infected but don’t develop any symptoms and don't feel unwell.
        Most people (about 80%) recover from the disease without needing special treatment.
        Around 1 out of every 6 people who gets COVID-19 becomes seriously ill and develops difficulty breathing.
        Older people, and those with underlying medical problems like high blood pressure, heart problems or diabetes, are more likely to develop serious illness.
        People with fever, cough and difficulty breathing should seek medical attention.`,
      },
      {
        title: 'How does COVID-19 spread?',
        answer: `People can catch COVID-19 from others who have the virus. The disease can spread from person to person through small droplets from the nose or mouth which are spread when a person with COVID-19 coughs or exhales. These droplets land on objects and surfaces around the person. Other people then catch COVID-19 by touching these objects or surfaces, then touching their eyes, nose or mouth. People can also catch COVID-19 if they breathe in droplets from a person with COVID-19 who coughs out or exhales droplets. This is why it is important to stay more than 1 meter (3 feet) away from a person who is sick.`,
      },
      {
        title:
          'Are antibiotics effective in preventing or treating the COVID-19?',
        answer: `No. Antibiotics do not work against viruses, they only work on bacterial infections. COVID-19 is caused by a virus, so antibiotics do not work. Antibiotics should not be used as a means of prevention or treatment of COVID-19. They should only be used as directed by a physician to treat a bacterial infection.`,
      },
      {
        title: 'Is there a vaccine, drug or treatment for COVID-19?',
        answer: `Not yet. To date, there is no vaccine and no specific antiviral medicine to prevent or treat COVID-2019. However, those affected should receive care to relieve symptoms. People with serious illness should be hospitalized. Most patients recover thanks to supportive care.            </div>`,
      },
      {
        title: 'Can I catch COVID-19 from my pet??',
        answer: `While there has been one instance of a dog being infected in Hong Kong, to date, there is no evidence that a dog, cat or any pet can transmit COVID-19. COVID-19 is mainly spread through droplets produced when an infected person coughs, sneezes, or speaks. To protect yourself, clean your hands frequently and thoroughly.`,
      },
      {
        title: 'Is COVID-19 airborne?',
        answer: `The virus that causes COVID-19 is mainly transmitted through droplets generated when an infected person coughs, sneezes, or speaks.These droplets are too heavy to hang in the air. They quickly fall on floors or surfaces.
                  You can be infected by breathing in the virus if you are within 1 metre of a person who has COVID-19, or by touching a contaminated surface and then touching your eyes, nose or mouth before washing your hands.`,
      },
    ];
  }
}
